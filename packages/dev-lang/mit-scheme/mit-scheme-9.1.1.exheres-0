# Copyright 2013 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=gz ]

SUMMARY="Implementation of the Scheme programming language"
DESCRIPTION="
MIT/GNU Scheme is an implementation of the Scheme programming language, providing an interpreter,
compiler, source-code debugger, integrated Emacs-like editor, and a large runtime library. MIT/GNU
Scheme is best suited to programming large applications with a rapid development cycle.
"
DOWNLOADS="
    mirror://gnu/${PN}/stable.pkg/${PV}/${PNV}.tar.gz
    bootstrap? (
        platform:amd64? ( mirror://gnu/${PN}/stable.pkg/${PV}/${PNV}-x86-64.tar.gz )
    )
"
LICENCES="${PN}" # GPL + openssl exception
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    bootstrap
    platform:
        amd64
"
DEPENDENCIES="
    build:
        !bootstrap? ( ${CATEGORY}/${PN} )
"

WORK=${WORKBASE}/${PNV}/src

src_unpack() {
    unpack ${PNV}.tar.gz
    if option bootstrap; then
        if option platform:amd64; then
            unpack ${PNV}-x86-64.tar.gz
        fi
    fi
}

# Tests are not shipped
src_test() { :; }

